## Página pessoal
 
Esse repositório contém um breve resumo sobre minhas informações pessoais, informações acadêmicas e experiências profissionais.
Foi criada para disciplina de desenvolvimento WEB I com o intuito de praticar conhecimentos de HTML, CSS e até mesmo de versionamento (git).

## Para rodar em sua máquina
1. Abra o git bash e execute o comando `git clone https://gitlab.com/gabrielcanto461/pagina-pessoal`;
2. Execute o comando `cd pagina-pessoal`;
3. Execute o comando `npm install`;
4. Execute o comando `npm start`;
5. Agora só desenvolver...
